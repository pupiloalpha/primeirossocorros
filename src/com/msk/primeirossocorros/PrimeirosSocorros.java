package com.msk.primeirossocorros;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;

public class PrimeirosSocorros extends Activity implements OnClickListener {

	private TextView pcr, animais, ferimentos, desmaios, queimaduras, ovace;
	private int nr;
	Bundle envelope;
	Intent atividade;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.inicio);

		queimaduras = (TextView) findViewById(R.id.tvQueimaduras);
		pcr = (TextView) findViewById(R.id.tvPCR);
		desmaios = (TextView) findViewById(R.id.tvDesmaios);
		animais = (TextView) findViewById(R.id.tvAnimais);
		ferimentos = (TextView) findViewById(R.id.tvFerimentos);
		ovace = (TextView) findViewById(R.id.tvOVACE);

		queimaduras.setOnClickListener(this);
		pcr.setOnClickListener(this);
		desmaios.setOnClickListener(this);
		animais.setOnClickListener(this);
		ferimentos.setOnClickListener(this);
		ovace.setOnClickListener(this);

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.primeiros_socorros, menu);
		return true;
	}

	@Override
	public void onClick(View botao) {

		switch (botao.getId()) {

		case R.id.tvQueimaduras:
			nr = 0;
			break;
		case R.id.tvPCR:
			nr = 1;
			break;
		case R.id.tvDesmaios:
			nr = 2;
			break;
		case R.id.tvAnimais:
			nr = 3;
			break;
		case R.id.tvFerimentos:
			nr = 4;
			break;
		case R.id.tvOVACE:
			nr = 5;
			break;
		}

		envelope = new Bundle();
		envelope.putInt("nr", nr);
		atividade = new Intent("com.msk.primeirossocorros.TIPOSPROCEDIMENTOS");
		atividade.putExtras(envelope);
		startActivity(atividade);
	}

}
