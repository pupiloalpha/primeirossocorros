package com.msk.primeirossocorros;

import android.app.Activity;
import android.content.res.Resources;
import android.os.Bundle;
import android.widget.TextView;

public class Procedimentos extends Activity {

	private TextView titulo, sintoma, procedimento;
	private String[] titulos, sintomas, procedimentos;
	private int[] corfundo;
	Resources r = null;
	Bundle envelope;
	private int nr;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.procedimentos);
		r = getResources();
		
		// BUSCA PROCEDIMENTOS
		envelope = getIntent().getExtras();
		nr = envelope.getInt("nr");
		
		titulo = (TextView) findViewById(R.id.tvTitulo);
		sintoma = (TextView) findViewById(R.id.tvSintoma);
		procedimento = (TextView) findViewById(R.id.tvProcedimento);
		
		titulos = r.getStringArray(R.array.titulos);
		
		DefineTexto(nr);
		
		corfundo = new int[] {
			R.color.Roxo, 		// 0
			R.color.Verde, 		// 1
			R.color.Amarelo, 	// 2
			R.color.Azul, 		// 3
			R.color.Escuro, 	// 4
			R.color.Vermelho, 	// 5
			R.color.Verde 		// 6
		};
		
	}

	private void DefineTexto(int i) {
		
		titulo.setText(titulos[i]);
		sintoma.setText(sintomas[i]);
		procedimento.setText(procedimentos[i]);
		
		//titulo.setBackgroundColor(corfundo[i]);
		
	}
	
	
	
	
}
