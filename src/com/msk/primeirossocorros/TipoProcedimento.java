package com.msk.primeirossocorros;

import android.annotation.SuppressLint;
import android.app.ListActivity;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

public class TipoProcedimento extends ListActivity {

	private TextView titulo;
	private LinearLayout barra;
	private String[] titulos, animais, pcr, desmaios, queimaduras, ferimentos, ovace;
	Resources r = null;
	private ArrayAdapter<String> adapter;
	private Bundle envelope;
	private int nr;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.tipos_procedimento);
		r = getResources();
		
		// BUSCA PROCEDIMENTOS
		envelope = getIntent().getExtras();
		nr = envelope.getInt("nr");
		
		titulo = (TextView) findViewById(R.id.tvTitulo);
		barra = (LinearLayout) findViewById(R.id.barraTitulo);

		BuscaDados();
		
		DefineTitulo(nr);
		
		DefineTipos(nr);
		
		setListAdapter(adapter);
	}


	private void BuscaDados() {
		titulos = r.getStringArray(R.array.titulos);
		animais = r.getStringArray(R.array.tipos_animais);
		pcr = r.getStringArray(R.array.tipos_pcr);
		desmaios = r.getStringArray(R.array.tipos_desmaios);
		queimaduras = r.getStringArray(R.array.tipos_queimaduras);
		ferimentos = r.getStringArray(R.array.tipos_ferimentos);
		ovace = r.getStringArray(R.array.tipos_ovace);
		
	}

	private void DefineTitulo(int i) {
		
		titulo.setText(titulos[i]);
		
	}
	
	@SuppressLint("ResourceAsColor")
	private void DefineTipos(int nrTipo) {
		
		switch (nrTipo) {
			
		case 0:
			adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, queimaduras);
			barra.setBackgroundResource(R.color.Amarelo);
			break;
		case 1:
			adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, pcr);
			barra.setBackgroundResource(R.color.Azul);
			break;
		case 2:
			adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, desmaios);
			barra.setBackgroundResource(R.color.Escuro);
			break;
		case 3:
			adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, animais);
			barra.setBackgroundResource(R.color.Verde);
			break;
		case 4:
			adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, ferimentos);
			barra.setBackgroundResource(R.color.Vermelho);
			break;
		case 5:
			adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, ovace);
			barra.setBackgroundResource(R.color.Roxo);
			break;
		
		}
		
	}
	@Override
	protected void onListItemClick(ListView l, View v, int position, long id) {
		//O QUE SERA FEITO QUANDO ESCOLHER CADA ITEM
		super.onListItemClick(l, v, position, id);
	}
	
	
}
